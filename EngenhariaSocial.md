# **Engenharia social - Guilherme Félix**

## Conceito

#### No contexto de segurança da informação, é uma técninca usada por criminosos para induzir usuários a enviar dados confidenciais, infectar seus computadores, ou abrir links de sites infectados. Explorando a ingenuidade ou falta de conhecimento de usuários desavisados.

## Como funciona?

#### Basicamente quase todos os ataques contém alguma engenharia social, como exemplo do clássico email de "phishing", basicamente esse ataque consiste em convencer o usuário de que o criminoso é uma fonte legítima, os emails contém vários anexos com vírus mas alegam ser confiáveis.

#### Outro exemplo e mais simples, um hacker pode frequentar algum local público tal como um shopping ou um edifício corporativo e bisbilhotar pessoas com tablets ou laptops. Muitas pessoas não tem a consciência de que poucas informações (nome, data de nascimento ou endereço), já prove acesso a diversas redes e após se disfarçarem de usuáios legítimos, é só uma questão de redefinir senhas e obter informações.

## Prevenção

#### A porteção contra esse tipo de técnica começa com o treinamento de usuários, eles devem ser direcionandos a nunca clicar em links suspeitos e a sempre proteger suas informações de login independente de onde estejam e possuir algum antivírus de alta performance.
